<div class="row">
    <!-- Blog Entries Column -->
    <div class="col-lg-12 col-md-12 px-0">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 lazy" data-src="/assets/img/banner/leawtaetaw-1.jpg" alt="First slide" style="object-fit:cover; max-height:40vh;">
                    <noscript>
                        <img class="d-block w-100" src="/assets/img/banner/leawtaetaw-1.jpg" alt="First slide" style="object-fit:cover; max-height:40vh;">
                    </noscript>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 lazy" data-src="/assets/img/banner/leawtaetaw-2.jpg" alt="Second slide" style="object-fit:cover; max-height:40vh;">
                    <noscript>
                        <img class="d-block w-100" src="/assets/img/banner/leawtaetaw-2.jpg" alt="Second slide" style="object-fit:cover; max-height:40vh;">
                    </noscript>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 lazy" data-src="/assets/img/banner/leawtaetaw-3.jpg" alt="Third slide" style="object-fit:cover; max-height:40vh;">
                    <noscript>
                        <img class="d-block w-100" src="/assets/img/banner/leawtaetaw-3.jpg" alt="Third slide" style="object-fit:cover; max-height:40vh;">
                    </noscript>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
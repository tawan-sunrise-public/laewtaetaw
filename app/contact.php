<div class="row">
    <div class="card w-100 pb-5 page-container">
        <div class="card-body">

            <div class="mb-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-chevron-right"></i> Contact us</li>
                    </ol>
                </nav>
            </div>

            <div class="heading-blog mb-4">
                <div class="heading">Contact us</div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12">

                    <div class="row">
                        <div class="col-12 mb-4">
                            <h4 class="text-center p-4">ติดต่องาน รีวิว โฆษณา</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            ติดต่องาน รีวิว โฆษณา ได้ที่ <br />
                            Tel. : (+66)88-226-9942 <br />
                            Line : nokpanumas <br />
                            Facebook: https://web.facebook.com/laewtaetaw/
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.row -->

<?php include './banner.php'; ?>

<div class="row mt-5">

    <div class="col-md-12">
        <div class="heading-blog mb-4">
            <div class="heading">โรงแรมที่พัก</div>
        </div>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/hotel/1.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/hotel/1.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</h2>
                    <p class="card-text">
                        จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น กาญจนบุรีเ...จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น กาญจนบุรีเ...จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น กาญจนบุรีเ...
                    </p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/hotel/2.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/hotel/2.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">เมนูอาหารไทย-อีสานสมัยใหม่ สุดแซ่บซี้ด ที่ Have a ซี๊ดดด</h2>
                    <p class="card-text">เมนูอาหารไทย-อีสานสมัยใหม่ กับเมนูสุดครีเอทหาได้ที่นี่ ที่ Have a Zeed</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/hotel/3.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/hotel/3.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">Chata Thammachart คาเฟ่วิวนา สุดฮิป</h2>
                    <p class="card-text">เดินทางออกจากกรุงเทพมุ่งหน้าสู่จังหวัดนครปฐม ไปนั่งชิล กับคาเฟ่สุดฮิปอย่าง Chata Thammachart กันดีกว่า</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/hotel/4.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/hotel/4.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">นึกถึงเวลาแห่งความสุข นึกถึง “สีฟ้า”</h2>
                    <p class="card-text">ความสุขมีได้ทุกที่ ต่างคนก็ต่างมีความสุขกันคนละแบบ คนละช่วงเวลา แต่ในห้วงเวลาใกล้วันขึ้นปีใหม่แบบนี้ เชื่อว่าความสุขของหลาย ๆ คนคงจะคล้าย...</p>
                </div>
            </div>
        </a>
    </div>

</div>
<!-- /.row -->

<div class="row mt-5">

    <div class="col-md-12">
        <div class="heading-blog mb-4">
            <div class="heading">ร้านอาหาร</div>
        </div>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/restaurant/1.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/restaurant/1.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">อย่าลืม Seefah เวลาหิว💙</h2>
                    <p class="card-text">เวลาหิวๆ เบื่ออาหารเดิมๆไปหมด ลองมาที่ร้านสีฟ้าสิ เขามีเมนูใหม่ สุดครีเอทมาให้เราลิ้มลองความอร่อยด้วยแหละจะเป็นเมนูอะไรนั้น ตามมาดูกันเล้ย</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/restaurant/2.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/restaurant/2.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">ซีฟู้ดรสชาติไทยแท้สูตรโบราณ สด อร่อย ที่กุ้งทอง ซีฟู๊ด พระราม4🍽🦑🦞🦀🍻</h2>
                    <p class="card-text">วันนี้จะมาแนะนำร้านอาหารอร่อยคุณภาพดีที่ต้องบอกต่อ ที่ร้านกุ้งทองซีฟู้ด ย่านพระราม 4</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/restaurant/3.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/restaurant/3.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">ไปทานอาหารสไตล์ฟิวชั่น ในบรรยากาศแบบนิทานจิ้งจอก ที่Fox’s Tale🦊🌈</h2>
                    <p class="card-text">เมื่อพูดถึงร้านอาหารแนวฟิวชั่น ทุกคนต้องไปลิ้มลองความอร่อย กับร้าน Fox’s Tale อยู่ใกล้MRT เตาปูนนี่เอง 🦊🦊</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mb-4">
        <!-- Blog Post -->
        <a href="?page=detail">
            <div class="card">
                <img class="card-img-top lazy" data-src="./assets/img/restaurant/4.jpg" alt="">
                <noscript>
                    <img class="card-img-top" src="./assets/img/restaurant/4.jpg" alt="">
                </noscript>
                <div class="card-body">
                    <h2 class="card-title">สาวแสนดีต้องไปสวรรค์ แต่สาวน่ารักแบบฉันต้องมา My Castle café</h2>
                    <p class="card-text">…อะแน่นอนว่าผู้หญิงน่ารักๆ อย่างเราก็ต้องไม่พลาดมาเช็คอินทีร้านสุดแสนหวาน สีชมพู๊ชมพู ที่นี่ให้ได้ 💗 💗 💗</p>
                </div>
            </div>
        </a>
    </div>

</div>
<!-- /.row -->
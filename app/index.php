<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LAEW TAE TAW</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Prompt&display=swap" rel="stylesheet">
    <!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style type="text/css">
        <?php include './css/main.css'; ?>body {
            padding-top: 56px;
            font-family: 'Prompt', sans-serif;
        }
    </style>
    <script>
        window.lazyLoadOptions = {
            elements_selector: '.lazy'
        }
    </script>
    <script async src="/js/lazyload.min.js"></script>
</head>

<body>

    <?php include './navigation.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <?php switch ($_GET['page']) {
            case 'list':
                include './list.php';
                break;
            case 'detail':
                include './detail.php';
                break;
            case 'vdo':
                include './vdo.php';
                break;
            case 'about':
                include './about.php';
                break;
            case 'contact':
                include './contact.php';
                break;
            default:
                include './home.php';
                break;
        } ?>

    </div>
    <!-- /.container -->

    <?php include './footer.php'; ?>
</body>

</html>
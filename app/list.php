<div class="row">
    <div class="card w-100 pb-5 page-container">
        <div class="card-body p-0 py-lg-2 blog">

            <!-- Sidebar -->
            <?php include './left-sidebar.php'; ?>

            <div class="blog-content">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-0 mb-lg-4">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-chevron-right"></i> ที่กิน</li>
                                </ol>
                            </nav>
                        </div>

                        <div class="heading-blog mb-4">
                            <div class="heading">Showing posts with label โรงแรมที่พัก. Show all posts</div>
                        </div>

                        <div class="col-12">
                        <div class="row">
                            <div class="col-12 mb-4">
                                <a href="?page=detail">
                                    <div class="card">
                                        <div class="row no-gutters">
                                            <div class="col-md-4">
                                                <img src="./assets/img/hotel/1.jpg" class="card-img" alt="..."/>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body py-4 py-lg-2">
                                                    <h5 class="card-title">[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</h5>
                                                    <p class="card-text">จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น กาญจนบุรีเ...</p>
                                                    <p class="card-text"><small class="text-muted"><i class="fa fa-calendar"></i> about a year ago</small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 mb-4">
                                <a href="?page=detail">
                                    <div class="card">
                                        <div class="row no-gutters">
                                            <div class="col-md-4">
                                                <img src="./assets/img/hotel/2.jpg" class="card-img" alt="..."/>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body py-4 py-lg-2">
                                                    <h5 class="card-title">เมนูอาหารไทย-อีสานสมัยใหม่ สุดแซ่บซี้ด ที่ Have a ซี๊ดดด</h5>
                                                    <p class="card-text">เมนูอาหารไทย-อีสานสมัยใหม่ กับเมนูสุดครีเอทหาได้ที่นี่ ที่ Have a Zeed</p>
                                                    <p class="card-text"><small class="text-muted"><i class="fa fa-calendar"></i> about a year ago</small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <?php for ($i = 1; $i <= 4; $i++) : ?>
                            <div class="row">
                                <div class="col-12 mb-4">
                                    <a href="?page=detail">
                                        <div class="card">
                                            <div class="row no-gutters">
                                                <div class="col-md-4">
                                                    <img src="./assets/img/hotel/3.jpg" class="card-img" alt="..."/>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-body py-4 py-lg-2">
                                                        <h5 class="card-title">[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</h5>
                                                        <p class="card-text">จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น กาญจนบุรีเ...</p>
                                                        <p class="card-text"><small class="text-muted"><i class="fa fa-calendar"></i> about a year ago</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 mb-4">
                                    <a href="?page=detail">
                                        <div class="card">
                                            <div class="row no-gutters">
                                                <div class="col-md-4">
                                                    <img src="./assets/img/hotel/4.jpg" class="card-img" alt="..."/>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-body py-4 py-lg-2">
                                                        <h5 class="card-title">[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</h5>
                                                        <p class="card-text">จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น กาญจนบุรีเ...</p>
                                                        <p class="card-text"><small class="text-muted"><i class="fa fa-calendar"></i> about a year ago</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endfor; ?>

                        <div class="row">
                            <div class="col-12 mb-4">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Sidebar -->
            <?php include './right-sidebar.php'; ?>

        </div>
    </div>
</div>
<!-- /.row -->
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">LAEWTAETAW</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="./">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=list">โรงแรม</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=list">อาหาร</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">ในประเทศ</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="?page=list">ภาคเหนือ</a>
                        <a class="dropdown-item" href="?page=list">ภาคใต้</a>
                        <a class="dropdown-item" href="?page=list">ภาคกลาง</a>
                        <a class="dropdown-item" href="?page=list">ภาคตะวันออก</a>
                        <a class="dropdown-item" href="?page=list">ภาคตะวันตก</a>
                        <a class="dropdown-item" href="?page=list">ภาคตะวันออกเฉียงเหนือ</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="?page=list">ทั้งหมด</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=list">ต่างประเทศ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=list">Cafe</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=list">Event</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=vdo">Video</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=about">About us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=contact">Contact us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=list">List Demo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=detail">Detail Demo</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
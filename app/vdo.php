<div class="row">
    <div class="card pb-5">
        <div class="card-body">

            <div class="mb-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-chevron-right"></i> ที่กิน</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-chevron-right"></i> [รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</li>
                    </ol>
                </nav>
            </div>

            <div class="heading-blog mb-4">
                <div class="heading">[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-8">

                    <div class="row">
                        <div class="col-12 mb-4">
                            <h4 class="p-4">[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด</h4>
                            <hr />
                            <div class="card" style="height:auto;width: fit-content;">
                                <div class="card-body"><i class="fa fa-calendar"></i> 6 months ago</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="post-body entry-content">
                                <img src="https://1.bp.blogspot.com/-81I9r37BJvk/Xs-FrJAJ8SI/AAAAAAAANQE/6Zj_T9_OGk8IUMHyH27pUJqgdsXKeHOrwCK4BGAsYHg/IMG_0799.JPG" style="display: none;">
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-81I9r37BJvk/Xs-FrJAJ8SI/AAAAAAAANQE/6Zj_T9_OGk8IUMHyH27pUJqgdsXKeHOrwCK4BGAsYHg/IMG_0799.JPG" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="1320" data-original-width="2048" height="420" src="https://1.bp.blogspot.com/-81I9r37BJvk/Xs-FrJAJ8SI/AAAAAAAANQE/6Zj_T9_OGk8IUMHyH27pUJqgdsXKeHOrwCK4BGAsYHg/w640-h412/IMG_0799.JPG" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    จากเหตุ Covid-19 ที่ทำให้เราต้องอยู่บ้าน อดเที่ยวกันเป็นเดือนๆ
                                    ผ่านมาจนหลายๆอย่างเริ่มดีขึ้น มีการผ่อนปลนกันมากขึ้น
                                    กาญจนบุรีเป็นจังหวัดแรกๆที่ผ่อนปลนให้เราได้เที่ยวกัน
                                    เราจึงเดินทางจากกรุงเทพเข้าไปเที่ยวที่กาญจนบุรี
                                </div>
                                <hr>
                                <div>
                                    ครั้งนี้เราพามาที่ The l ดิ แอตติค คาเฟ่น่ารักๆ มินิมอล@กาญจนบุรี<br>คาเฟ่ที่ออกแบบเป็นห้องใต้หลังคาสีขาว
                                    ตกแต่งร้านแบบสไตล์นอร์ดิก เน้นโทนขาวสว่างให้ความรู้สึกนุ่มนวลอบอุ่น
                                    คุมโทนด้วยต้นไม้สีเขียวสบายตา
                                </div>
                                <div><br></div>
                                <div>
                                    Outdoor มุมถ่ายรูปสวยเพียบ ไฮไลน์ก็ต้องเป็นมุมจากหน้าต่างหลังคา
                                    รับกับแสงท้องฟ้าสวยๆ
                                    แถมทุ่งหญ้าด้านนอกถ่ายรูปออกมาได้ชิคอย่างกะอยู่ต่างประเทศเลยทีเดียว
                                </div>ร้านติดถนนใหญ่ ติดกับโชว์รูมโตโยต้า มีลานจอดรถกว้างขวาง เดินทางสะดวก ไม่ว่าจะแวะก่อนขึ้นไปเที่ยว หรือ แวะขากลับก็สะดวกทั้งนั้น<div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-vCFJWZv8gqY/Xs-T00nboFI/AAAAAAAANRs/6yn4QPhw-mwquEDIUZ2OlqXQbLiEtb5vwCK4BGAsYHg/TheAttic-8.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3098" data-original-width="4721" height="420" src="https://1.bp.blogspot.com/-vCFJWZv8gqY/Xs-T00nboFI/AAAAAAAANRs/6yn4QPhw-mwquEDIUZ2OlqXQbLiEtb5vwCK4BGAsYHg/w640-h420/TheAttic-8.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>บรรยากาศรอบๆคาเฟ่ สามารถหามุมถ่ายรูปได้สบายๆ</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-MSyFZCMztfc/Xs-T3T8vE7I/AAAAAAAANR0/kv3bB3TOlkc8GWDrmhRDIs488dtAgpCSACK4BGAsYHg/TheAttic-6.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4675" height="446" src="https://1.bp.blogspot.com/-MSyFZCMztfc/Xs-T3T8vE7I/AAAAAAAANR0/kv3bB3TOlkc8GWDrmhRDIs488dtAgpCSACK4BGAsYHg/w640-h446/TheAttic-6.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-GbqJvBamTrI/Xs-SvolmWsI/AAAAAAAANRE/Vwx57gRznSUtr61SbY2PBdjV4Xr4C1d-wCK4BGAsYHg/TheAttic-3.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-GbqJvBamTrI/Xs-SvolmWsI/AAAAAAAANRE/Vwx57gRznSUtr61SbY2PBdjV4Xr4C1d-wCK4BGAsYHg/w640-h426/TheAttic-3.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-ILQUTvdp13c/Xs-SwtD3GTI/AAAAAAAANRI/-hJ0ipXK4-8RM8E1RgC8gS9uZrVqxExkQCK4BGAsYHg/TheAttic-7.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4703" height="444" src="https://1.bp.blogspot.com/-ILQUTvdp13c/Xs-SwtD3GTI/AAAAAAAANRI/-hJ0ipXK4-8RM8E1RgC8gS9uZrVqxExkQCK4BGAsYHg/w640-h444/TheAttic-7.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    ที่นั่งด้านนอกรับลม มองออกไปเห็นพื้นสนามหญ้า กับระเบียงสีขาว
                                    ก็ถ่ายรูปสวยๆได้ไม่เบา
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-kzPsviI2Sb8/Xs-SxkL5ZWI/AAAAAAAANRM/whCVMZwVDzgCxTitnF1xA9OWlEkwgcPxACK4BGAsYHg/TheAttic-21.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-kzPsviI2Sb8/Xs-SxkL5ZWI/AAAAAAAANRM/whCVMZwVDzgCxTitnF1xA9OWlEkwgcPxACK4BGAsYHg/w640-h426/TheAttic-21.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-bj-whS-lrGo/Xs-Sysq-DzI/AAAAAAAANRQ/pWe1eNtyjC49wVNT48aDDZHh0voy9tsgACK4BGAsYHg/TheAttic-22.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-bj-whS-lrGo/Xs-Sysq-DzI/AAAAAAAANRQ/pWe1eNtyjC49wVNT48aDDZHh0voy9tsgACK4BGAsYHg/w640-h426/TheAttic-22.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-vHw5hQWSII8/Xs-SzgRfvvI/AAAAAAAANRU/kxYOcBoXFE8vitQnJvnq306uFPdMglaNgCK4BGAsYHg/TheAttic-117.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-vHw5hQWSII8/Xs-SzgRfvvI/AAAAAAAANRU/kxYOcBoXFE8vitQnJvnq306uFPdMglaNgCK4BGAsYHg/w640-h426/TheAttic-117.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-oWqHNeprAeI/Xs-S0lSITcI/AAAAAAAANRY/JLXGZOx3VigZyQZhT2bw6U5CpjOqGEZawCK4BGAsYHg/TheAttic-118.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-oWqHNeprAeI/Xs-S0lSITcI/AAAAAAAANRY/JLXGZOx3VigZyQZhT2bw6U5CpjOqGEZawCK4BGAsYHg/w640-h426/TheAttic-118.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-brZnu1fAC60/Xs-S1ure6YI/AAAAAAAANRc/EY8YrXX3g1kV-EfH0Z7rBCUau7mKK9gxACK4BGAsYHg/TheAttic-129.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-brZnu1fAC60/Xs-S1ure6YI/AAAAAAAANRc/EY8YrXX3g1kV-EfH0Z7rBCUau7mKK9gxACK4BGAsYHg/w640-h426/TheAttic-129.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-bPlF2vfZe7U/Xs-S240-mpI/AAAAAAAANRg/yTuWUSIVDq8ArwzjajLeE3_JrPn-3EY1wCK4BGAsYHg/TheAttic-126.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-bPlF2vfZe7U/Xs-S240-mpI/AAAAAAAANRg/yTuWUSIVDq8ArwzjajLeE3_JrPn-3EY1wCK4BGAsYHg/w640-h426/TheAttic-126.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-5S-ZMkKiees/Xs-S3zJsA2I/AAAAAAAANRk/zllbE-ZarjAW9ZeIJ15EqWlVmsNLnr2agCK4BGAsYHg/TheAttic-128.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-5S-ZMkKiees/Xs-S3zJsA2I/AAAAAAAANRk/zllbE-ZarjAW9ZeIJ15EqWlVmsNLnr2agCK4BGAsYHg/w640-h426/TheAttic-128.jpg" width="640"></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: center;"><br></div>
                                <div></div>
                                <div>
                                    Indoor ภายในโล่ง เป็นห้องกระจกรับแสง จัดมุมโต๊ะหลากหลายมองเห็นธรรมชาติ
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-m0rMIooCgV8/Xs-GTRdoVbI/AAAAAAAANQc/MVBFBLYJMAMGDhuvJzxHGJoHfDe57glnQCK4BGAsYHg/TheAttic-16.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-m0rMIooCgV8/Xs-GTRdoVbI/AAAAAAAANQc/MVBFBLYJMAMGDhuvJzxHGJoHfDe57glnQCK4BGAsYHg/w640-h426/TheAttic-16.jpg" width="640"></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: center;"><br></div>
                                <div class="separator" style="clear: both; text-align: left;">
                                    ร้านตกแต่งสไตล์นอร์ดิก Nordic Style เป็นแนวคิดที่อยู่ร่วมกับธรรมชาติ
                                    ตกแต่งโทนสีขาว โปร่ง สบายตา ติดกระจกรับแสงธรรมชาติ
                                    ประดับต้นไม้สีเขียวเข้มภายใน
                                </div>
                                <div class="separator" style="clear: both; text-align: left;"><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-4VYyiIgjYPA/Xs-SKF0uOnI/AAAAAAAANQo/cbJ_Nne46G4eahxR3nrTMWsIN_yaMHzMACK4BGAsYHg/TheAttic-20.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-4VYyiIgjYPA/Xs-SKF0uOnI/AAAAAAAANQo/cbJ_Nne46G4eahxR3nrTMWsIN_yaMHzMACK4BGAsYHg/w640-h426/TheAttic-20.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-oRItYtIQkqs/Xs-SLfyXzUI/AAAAAAAANQs/lfVCMoEOr2I_I-_I_Nb4tQRl3xNy-0z-gCK4BGAsYHg/TheAttic-24.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-oRItYtIQkqs/Xs-SLfyXzUI/AAAAAAAANQs/lfVCMoEOr2I_I-_I_Nb4tQRl3xNy-0z-gCK4BGAsYHg/w640-h426/TheAttic-24.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-gc-HuzJR_e0/Xs-SMTDjFvI/AAAAAAAANQw/nt23fmOAAD0_9QVKNoEKp5AkxGEdvMKMwCK4BGAsYHg/TheAttic-25.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-gc-HuzJR_e0/Xs-SMTDjFvI/AAAAAAAANQw/nt23fmOAAD0_9QVKNoEKp5AkxGEdvMKMwCK4BGAsYHg/w640-h426/TheAttic-25.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-hWcMRbPqRl4/Xs-SNFDsq7I/AAAAAAAANQ0/u_Ty2Hb-N584sE6I1BmelFMt-U-jr0oOQCK4BGAsYHg/TheAttic-28.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3227" data-original-width="4896" height="422" src="https://1.bp.blogspot.com/-hWcMRbPqRl4/Xs-SNFDsq7I/AAAAAAAANQ0/u_Ty2Hb-N584sE6I1BmelFMt-U-jr0oOQCK4BGAsYHg/w640-h422/TheAttic-28.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-LLoMUsnIXKg/Xs-SOTWMOpI/AAAAAAAANQ4/N1QmUkI4fxUrNJkxKSsgnv_fLKVSFo_vQCK4BGAsYHg/TheAttic-29.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-LLoMUsnIXKg/Xs-SOTWMOpI/AAAAAAAANQ4/N1QmUkI4fxUrNJkxKSsgnv_fLKVSFo_vQCK4BGAsYHg/w640-h426/TheAttic-29.jpg" width="640"></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: center;"><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-_4-ptGtakag/Xs-SShcUCrI/AAAAAAAANQ8/nfv6da_bfus7tYoBqbRwI-p2YKrCUkcCACK4BGAsYHg/TheAttic-58.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3124" data-original-width="4883" height="410" src="https://1.bp.blogspot.com/-_4-ptGtakag/Xs-SShcUCrI/AAAAAAAANQ8/nfv6da_bfus7tYoBqbRwI-p2YKrCUkcCACK4BGAsYHg/w640-h410/TheAttic-58.jpg" width="640"></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: center;"><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-YJWCW2JLApg/Xs-bCTzJGxI/AAAAAAAANUY/XC1RlueQCRwnKD3iUB4KY83EL-3ETveiQCK4BGAsYHg/TheAttic-57.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-YJWCW2JLApg/Xs-bCTzJGxI/AAAAAAAANUY/XC1RlueQCRwnKD3iUB4KY83EL-3ETveiQCK4BGAsYHg/w640-h426/TheAttic-57.jpg" width="640"></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: center;"></div>
                                <div class="separator" style="clear: both; text-align: left;"><br></div>
                                <div>เมนูที่เราสั่งมาในวันนี้กันบ้างดีกว่า</div>
                                <div>อ้อ! ทางร้านใช้แก้วที่เป็นมิตรต่อสิ่งแวดล้อม ย่อยสลายได้ด้วย</div>
                                <div><br></div>
                                <div>
                                    <b>Koi Matcha Espresso (75 บาท)<br></b>
                                </div>
                                <div>
                                    ชาเขียวมัทฉะเกรดพรีเมียมจากญี่ปุ่นผสมกับความนุ่ม กลมกล่อมของกาแฟคั่วกลาง<br>
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-GNCgt_hiuTU/Xs-Una6HxrI/AAAAAAAANR8/Nulo2wNeQ4kTEAN39uUFow1GFjDijsCHQCK4BGAsYHg/TheAttic-42.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-GNCgt_hiuTU/Xs-Una6HxrI/AAAAAAAANR8/Nulo2wNeQ4kTEAN39uUFow1GFjDijsCHQCK4BGAsYHg/w640-h426/TheAttic-42.jpg" width="640"><b><br></b></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: left;">
                                    <b><br></b>
                                </div>
                                <b>Koi Matcha</b> และ <b>Dark Chocolate (70 บาท)</b><br>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-zG4i65MiQdU/Xs-WbOD8CWI/AAAAAAAANSE/J80iYH2kQoQ9pmVuQrPVU78zw6Sdy6DygCK4BGAsYHg/TheAttic-75.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="2979" data-original-width="4896" height="390" src="https://1.bp.blogspot.com/-zG4i65MiQdU/Xs-WbOD8CWI/AAAAAAAANSE/J80iYH2kQoQ9pmVuQrPVU78zw6Sdy6DygCK4BGAsYHg/w640-h390/TheAttic-75.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>มีเครื่องดื่มก็ต้องตามด้วยของหวานนน</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-C5AQD94B4cs/Xs-XH4d35UI/AAAAAAAANSM/LFVi9QKjjw05Py_62MOwVGwuZQJVzLKIACK4BGAsYHg/TheAttic-59.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-C5AQD94B4cs/Xs-XH4d35UI/AAAAAAAANSM/LFVi9QKjjw05Py_62MOwVGwuZQJVzLKIACK4BGAsYHg/w640-h426/TheAttic-59.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-rIeG5Y9kJYY/Xs-XIle0k9I/AAAAAAAANSQ/7dAoj5Q0t3Ii_rGPRmvJ8xKML-m2ExDFwCK4BGAsYHg/TheAttic-73.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-rIeG5Y9kJYY/Xs-XIle0k9I/AAAAAAAANSQ/7dAoj5Q0t3Ii_rGPRmvJ8xKML-m2ExDFwCK4BGAsYHg/w640-h426/TheAttic-73.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    <b>Blueberry cheese cake</b><br>ตัวชีสหอมรสชาติกำลังดี ตัวเเครกเกอร์แน่นกรุบ
                                    รสบลูเบอรี่หวานๆ อร่อยเพลิน
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-sIYGCB7KaC4/Xs-XJ39IqNI/AAAAAAAANSU/3ceZPmqS7AgZn2h5Hiqdav6Wjq5-YrRdACK4BGAsYHg/TheAttic-76.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-sIYGCB7KaC4/Xs-XJ39IqNI/AAAAAAAANSU/3ceZPmqS7AgZn2h5Hiqdav6Wjq5-YrRdACK4BGAsYHg/w640-h426/TheAttic-76.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    <b>Red velvet cake</b><br>เค้กสไตล์โปรด เนื้อเค้กสีแดงตัดกับครีมชีส
                                    หวานหอมโดนใจ แอดชอบจานของร้านนี้เป็นเซรามิกสีสันตัดกับเค้ก
                                    ยิ่งทำให้โดดเด่นและน่ารับประทาน แถมถ่ายรูปก็สวย
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-pH3Dn382GXc/Xs-XK2SNbmI/AAAAAAAANSY/zdpHQniCzx0Z3PkmtZICI7YiSCET5J5IACK4BGAsYHg/TheAttic-77.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-pH3Dn382GXc/Xs-XK2SNbmI/AAAAAAAANSY/zdpHQniCzx0Z3PkmtZICI7YiSCET5J5IACK4BGAsYHg/w640-h426/TheAttic-77.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    <b>Lemon cream cheese pie</b><br>เลม่อนหวานอมเปรี้ยวนิดๆ
                                    กับครีมชีสหอมเค็มมัน เข้ากันมากๆ
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-QIIBcPydQZI/Xs-XLsHfufI/AAAAAAAANSc/pKa25HMG6pAmXwnF12NLBJYVe8Qhm96DwCK4BGAsYHg/TheAttic-69.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-QIIBcPydQZI/Xs-XLsHfufI/AAAAAAAANSc/pKa25HMG6pAmXwnF12NLBJYVe8Qhm96DwCK4BGAsYHg/w640-h426/TheAttic-69.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div><b>Fudge Brownie</b><br>เนื้อแน่น ฉ่ำ หนึบหนับ ช็อกโกเข้ม เลิฟ</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-X7hcv6_F62k/Xs-XM18hLRI/AAAAAAAANSg/6KXoWaCUvOIb7WvjtDIlLpqeikdKKAQkgCK4BGAsYHg/TheAttic-70.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-X7hcv6_F62k/Xs-XM18hLRI/AAAAAAAANSg/6KXoWaCUvOIb7WvjtDIlLpqeikdKKAQkgCK4BGAsYHg/w640-h426/TheAttic-70.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    <b>Caramelised Pudding</b><br>คาราเมลพุดดิ้ง เนื้อดึ๋งๆๆ
                                    ราดด้วยคาราเมลที่มีการเบิร์นทำให้มีรสขมนิดๆ รสชาติไม่หวานเลี่ยน
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-Ydj-Fvsx4Uk/Xs-Y0W-RZXI/AAAAAAAANTE/1nfgnRkg-wEnazKqE4mLzXV8QL5E0s2xgCK4BGAsYHg/TheAttic-85.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-Ydj-Fvsx4Uk/Xs-Y0W-RZXI/AAAAAAAANTE/1nfgnRkg-wEnazKqE4mLzXV8QL5E0s2xgCK4BGAsYHg/w640-h426/TheAttic-85.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>พุดดิ้งเนื้อเนียน เด้งดึ๋งๆสู้ช้อน55</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-gcUftwsl8xA/Xs-Y1Vjz0II/AAAAAAAANTI/t1VecgYu9jk_i48hIQKjVHBbj_bElBangCK4BGAsYHg/TheAttic-92.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-gcUftwsl8xA/Xs-Y1Vjz0II/AAAAAAAANTI/t1VecgYu9jk_i48hIQKjVHBbj_bElBangCK4BGAsYHg/w640-h426/TheAttic-92.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>มาที่ Seasonal menu อย่าง<br>Yuzu Americano และ Yuzu Fizz</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-VXJGbTRf_Kg/Xs-Y2SjdnLI/AAAAAAAANTM/zbD3cJWLjXsRY7EHe8sVhB-tOd8bbID9QCK4BGAsYHg/TheAttic-109.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-VXJGbTRf_Kg/Xs-Y2SjdnLI/AAAAAAAANTM/zbD3cJWLjXsRY7EHe8sVhB-tOd8bbID9QCK4BGAsYHg/w640-h426/TheAttic-109.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-KEuChCf9wAU/Xs-Y3mF_F4I/AAAAAAAANTQ/hf124VibglIjgOw9lcAYI1M8V4YaKoT8wCK4BGAsYHg/TheAttic-108.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-KEuChCf9wAU/Xs-Y3mF_F4I/AAAAAAAANTQ/hf124VibglIjgOw9lcAYI1M8V4YaKoT8wCK4BGAsYHg/w640-h426/TheAttic-108.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    <b>Yuzu Americano</b><br>น้ำส้มยูสุหวานหอมสดชื่นตัดกับความเข้มของรสชาติกาแฟ
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-YPzMKjzztWA/Xs-Y4nPHhJI/AAAAAAAANTU/5S1nOLmjuGg7x-4gDSGBM6dzCrB5A777QCK4BGAsYHg/TheAttic-104.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-YPzMKjzztWA/Xs-Y4nPHhJI/AAAAAAAANTU/5S1nOLmjuGg7x-4gDSGBM6dzCrB5A777QCK4BGAsYHg/w640-h426/TheAttic-104.jpg" width="640"></a>
                                </div>
                                <div>
                                    <b><br></b>
                                </div>
                                <div>
                                    <b>Yuzu Fizz</b><br>น้ำส้มยูสุพระเอกของเราผสมกับโซดา เปรี้ยวหวานซ่า
                                    ทานเเล้วดับร้อนได้ดีจริงๆ
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-A_Q4KW3RDrw/Xs-Y5qjuN9I/AAAAAAAANTY/brau1AWvGjkEgpQwdFi6w5G0f4I4r7DqgCK4BGAsYHg/TheAttic-114.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="4896" data-original-width="3264" height="640" src="https://1.bp.blogspot.com/-A_Q4KW3RDrw/Xs-Y5qjuN9I/AAAAAAAANTY/brau1AWvGjkEgpQwdFi6w5G0f4I4r7DqgCK4BGAsYHg/w426-h640/TheAttic-114.jpg" width="426"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    ก่อนกลับต้องได้รูปมุมนี้นะ<br>ถือเป็นซิกเนเจอร์ของคาเฟ่ห้องใต้หลังคานี้เลยแหละ
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-bzRX8nq_6aA/Xs-Z2bdkg7I/AAAAAAAANTg/OnTvqdu8xfcDLcOxz9X8NCQ3vaUEUizcwCK4BGAsYHg/202005274478122694433639924.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3158" data-original-width="4636" height="436" src="https://1.bp.blogspot.com/-bzRX8nq_6aA/Xs-Z2bdkg7I/AAAAAAAANTg/OnTvqdu8xfcDLcOxz9X8NCQ3vaUEUizcwCK4BGAsYHg/w640-h436/202005274478122694433639924.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-u1G1LgfEyKw/Xs-Z3iCOs2I/AAAAAAAANTk/NdbaurXxwwkzMm8FPekuNAE7uf0_SqaPgCK4BGAsYHg/202005278683213226444780820.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3238" data-original-width="4857" height="426" src="https://1.bp.blogspot.com/-u1G1LgfEyKw/Xs-Z3iCOs2I/AAAAAAAANTk/NdbaurXxwwkzMm8FPekuNAE7uf0_SqaPgCK4BGAsYHg/w640-h426/202005278683213226444780820.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-FZXwPG04sT0/Xs-Z5VlBUhI/AAAAAAAANTo/Lv2GjSt7doEgEkX0ObqQSEwxWipQg2MHACK4BGAsYHg/202005278641604305824807978.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="1497" data-original-width="1951" height="492" src="https://1.bp.blogspot.com/-FZXwPG04sT0/Xs-Z5VlBUhI/AAAAAAAANTo/Lv2GjSt7doEgEkX0ObqQSEwxWipQg2MHACK4BGAsYHg/w640-h492/202005278641604305824807978.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>คาวาอี้</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-tfGHKeStEVw/Xs-Z6dpHGOI/AAAAAAAANTs/jfnNttUOpwco64g7c3T76qgEgEHiZ-PTQCK4BGAsYHg/TheAttic-126.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-tfGHKeStEVw/Xs-Z6dpHGOI/AAAAAAAANTs/jfnNttUOpwco64g7c3T76qgEgEHiZ-PTQCK4BGAsYHg/w640-h426/TheAttic-126.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>มุมมินิมอล</div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-83MtXeCcCfI/Xs-Z7qHJAvI/AAAAAAAANTw/diVZMyPI9wkjxPzeyixwwbDNaUnmNdboACK4BGAsYHg/TheAttic-26.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-83MtXeCcCfI/Xs-Z7qHJAvI/AAAAAAAANTw/diVZMyPI9wkjxPzeyixwwbDNaUnmNdboACK4BGAsYHg/w640-h426/TheAttic-26.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-EfpUOFgrE5U/Xs-ai1IWOYI/AAAAAAAANUM/qWUGu_iVq_oA9YI38Zi3K_Zslnyv8M_SwCK4BGAsYHg/TheAttic-37.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-EfpUOFgrE5U/Xs-ai1IWOYI/AAAAAAAANUM/qWUGu_iVq_oA9YI38Zi3K_Zslnyv8M_SwCK4BGAsYHg/w640-h426/TheAttic-37.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-1ofmw843cR0/Xs-Z81kRCRI/AAAAAAAANT0/j_fpT6joQyslMO7egeWVQbEiik0E-i8qQCK4BGAsYHg/TheAttic-34.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3187" data-original-width="4654" height="438" src="https://1.bp.blogspot.com/-1ofmw843cR0/Xs-Z81kRCRI/AAAAAAAANT0/j_fpT6joQyslMO7egeWVQbEiik0E-i8qQCK4BGAsYHg/w640-h438/TheAttic-34.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>
                                    ก่อนกลับแวะดูสินค้าแบรนด์ร้าน The Attic อาทิ ถุงผ้า แก้วน้ำ
                                    กระเป๋าผ้าใบเล็กน่ารักๆ เป็นต้น
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-iZMB5-az-P8/Xs-Z-C2rD8I/AAAAAAAANT4/HodiWw18LeEOL_6t4xLnUjsQXbTwBreswCK4BGAsYHg/TheAttic-120.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-iZMB5-az-P8/Xs-Z-C2rD8I/AAAAAAAANT4/HodiWw18LeEOL_6t4xLnUjsQXbTwBreswCK4BGAsYHg/w640-h426/TheAttic-120.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-_xJyV_vj8tg/Xs-Z_Y5GJCI/AAAAAAAANT8/ig8kY9wJkhkd6kQDaCB-mTw7K6TRrwGlwCK4BGAsYHg/TheAttic-123.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-_xJyV_vj8tg/Xs-Z_Y5GJCI/AAAAAAAANT8/ig8kY9wJkhkd6kQDaCB-mTw7K6TRrwGlwCK4BGAsYHg/w640-h426/TheAttic-123.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-AHYyJ9yiH5g/Xs-aAWCKB1I/AAAAAAAANUA/Mc0St3Ey4g8SUykTBiR03CAO_DezNtGyACK4BGAsYHg/TheAttic-122.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-AHYyJ9yiH5g/Xs-aAWCKB1I/AAAAAAAANUA/Mc0St3Ey4g8SUykTBiR03CAO_DezNtGyACK4BGAsYHg/w640-h426/TheAttic-122.jpg" width="640"></a>
                                </div>
                                <br>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-J42HuESevR8/Xs-aBvqADiI/AAAAAAAANUE/k7jy_Ve2pUczdeeRMtnrkY_Ggob7JP1-QCK4BGAsYHg/TheAttic-124.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="2297" data-original-width="3212" height="458" src="https://1.bp.blogspot.com/-J42HuESevR8/Xs-aBvqADiI/AAAAAAAANUE/k7jy_Ve2pUczdeeRMtnrkY_Ggob7JP1-QCK4BGAsYHg/w640-h458/TheAttic-124.jpg" width="640"></a>
                                </div>
                                <div class="separator" style="clear: both; text-align: center;"><br></div>
                                <div>
                                    The Attic เป็นอีกหนึ่งคาเฟ่ที่ลงตัว คอกาแฟ สายหวาน สายถ่ายรูป ไม่ควรพลาด
                                </div>
                                <div><br></div>
                                <div class="separator" style="clear: both; text-align: center;">
                                    <a href="https://1.bp.blogspot.com/-gaaXL7sS9dE/Xs-a1WLF66I/AAAAAAAANUU/bI0zt0PGR-sDovZ4-SXnD0z3QeQw2DT_QCK4BGAsYHg/TheAttic-5.jpg" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="3264" data-original-width="4896" height="426" src="https://1.bp.blogspot.com/-gaaXL7sS9dE/Xs-a1WLF66I/AAAAAAAANUU/bI0zt0PGR-sDovZ4-SXnD0z3QeQw2DT_QCK4BGAsYHg/w640-h426/TheAttic-5.jpg" width="640"></a>
                                </div>
                                <div><br></div>
                                <div>เมนูแนะนำ<br>Dark choc หวานน้อย อร่อยลงตัวเข้มข้น<br>Blueberry cheese cake อันนี้ต้องโดน มีต่อวันไม่เยอะ มาช้าหมดนะจ๊ะ<br>Brownie อันนี้ก้ต้องโดน เปน Signature ของร้านเลยก็ว่าได้<br><br>คะแนน<br>บรรยากาศ 10/10 บรรยากาศดี วิวสวย เอาคะแนนเต็มไปเลยข้อนี้<br>รสชาติ 9.5/10 หวานน้อย คนชอบหวานอาจจะรู้สึกว่าไม่ค่อยหวาน แต่ทางร้านมีไซรัปให้เติมตามใจชอบได้เลย<br>ราคา 10/10 อยากจะให้ 11 คะแนน ราคามิตรภาพดีงามมาก <br>ความพึงพอใจโดยรวม&nbsp; 9.9 โดยรวมถือว่าดี ขอหัก 0.1 คะแนนตรงที่ห้องน้ำเดินไกลไปนิด<br></div>
                                <div><br></div>
                                <div>
                                    ⏱ร้านเปิดบริการทุกวัน 09.00-18.00 น.<br>🚗มีลานจอดรถ<br>📍พิกัด
                                    <a href="https://goo.gl/maps/kCr3LPdXvjUKmKHE8" target="_blank">https://goo.gl/maps/kCr3LPdXvjUKmKHE8</a><br>💬Facebook
                                    <a href="https://www.facebook.com/theattickan" target="_blank">The Attic l ดิ แอตติค</a><br>📱เบอร์โทร 062 560 8003<br>
                                </div>

                                <div>
                                    <br>
                                    <br>
                                    ติดตามพวกเราได้ที่ :<span style="font-family: sans-serif; text-align: start;"> <a href="้https://www.facebook.com/laewtaetaw" target="_blank">แล้วแต่ตัว</a></span><br>
                                    รีวิวอื่นๆ: <span style="font-family: sans-serif; text-align: start;"> <a href="https://www.laewtaetaw.com/" target="_blank"><span style="color: blue;">คลิก</span> </a></span>
                                </div>
                                <div style="clear: both;"></div>

                                *** เอาจากระบบเก่า
                                <div class="sharepost">
                                    <ul>
                                        <li><a class="twitter" href="http://twitter.com/share?url=https://www.laewtaetaw.com/2020/05/attic.html" rel="nofollow" target="_blank" title="Twitter Tweet"><i class="fa fa-twitter"></i>Tweet</a></li>
                                        <li><a class="facebook" href="http://www.facebook.com/sharer.php?u=https://www.laewtaetaw.com/2020/05/attic.html" rel="nofollow" target="_blank" title="Facebook Share"><i class="fa fa-facebook"></i>Share</a></li>
                                        <li><a class="gplus" href="http://plus.google.com/share?url=https://www.laewtaetaw.com/2020/05/attic.html" rel="nofollow" target="_blank" title="Google Plus Share"><i class="fa fa-google-plus"></i>Share</a></li>
                                        <li><a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.laewtaetaw.com/2020/05/attic.html&amp;title=[รีวิว] The Attic มินิมอลคาเฟ่ต์สไตล์นอร์ดิก Landmark ใหม่แห่งกาญจนบุรี มาเมืองกาญต้องห้ามพลาด&amp;summary=" target="_blank"><i class="fa fa-linkedin"></i>Share</a></li>
                                        <li><a class="pinterest" href="http://pinterest.com/pin/create/button/?url=https://www.laewtaetaw.com/2020/05/attic.html&amp;media=https://1.bp.blogspot.com/-81I9r37BJvk/Xs-FrJAJ8SI/AAAAAAAANQE/6Zj_T9_OGk8IUMHyH27pUJqgdsXKeHOrwCK4BGAsYHg/s72-c/IMG_0799.JPG&amp;description= + data:post.title" target="_blank"><i class="fa fa-pinterest"></i>Share</a></li>
                                    </ul>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Sidebar -->
                <?php include './sidebar.php'; ?>
            </div>

            <div class="row">
                <div class="col-6">
                    PREVIOUS
                </div>
                <div class="col-6">
                    NEXT
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.row -->